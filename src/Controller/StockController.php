<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Stock;

class StockController extends Controller
{
    private $serializer;
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }


    /**
     * @Route("/", methods= "GET" )
     */


     public function getAll() {
        $repo = $this->getDoctrine()->getRepository(Stock::class);

        $json = $this->serializer->serialize($repo->findAll(), "json");

        return JsonResponse::fromJsonString($json);

     }

     /**
     * @Route("/stock", methods={"POST"})
     */
    public function add(Request $request)
    {
        $body = $request->getContent();

        $stock = $this->serializer->deserialize($body, Stock::class, "json");

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($stock);
        $manager->flush();

        return new JsonResponse(["id" => $stock->getId()]);

    }

     /**
     * @Route("/stock/{stock}", methods={"DELETE"})
     */
    public function delete(Stock $stock)
    { 
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($stock);
        $manager->flush();
        return new Response("ok", 204);
    } 

     /**
     * @Route("/stock/{stock}", methods={"PUT"})
     */
    public function update(Stock $stock, Request $request)
    {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Stock::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $stock->setMontant($updated->getMontant());
        $stock->setTag($updated->getTag());
        $stock->setDatetime($updated->getDatetime());
        $stock->setUser($updated->getUser());

        $manager->flush();

        $data = $this->serializer->normalize($stock, null, 
        ['attributes' => ['id', 'montant', 'tag', 'datetime', 'user']]);

        return new Response($this->serializer->serialize($data, 'json'));
    } 
}
