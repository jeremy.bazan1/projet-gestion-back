<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Table;

class TableController extends Controller
{
    private $serializer;
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }


    /**
     * @Route("/", methods= "GET" )
     */


     public function getAll() {
        $repo = $this->getDoctrine()->getRepository(Table::class);

        $json = $this->serializer->serialize($repo->findAll(), "json");

        return JsonResponse::fromJsonString($json);

     }

     /**
     * @Route("/table", methods={"POST"})
     */
    public function add(Request $request)
    {
        $body = $request->getContent();

        $table = $this->serializer->deserialize($body, Table::class, "json");

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($table);
        $manager->flush();

        return new JsonResponse(["id" => $table->getId()]);

    }

     /**
     * @Route("/table/{table}", methods={"DELETE"})
     */
    public function delete(Table $table)
    { 
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($table);
        $manager->flush();
        return new Response("ok", 204);
    } 

     /**
     * @Route("/table/{table}", methods={"PUT"})
     */
    public function update(Table $table, Request $request)
    {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Table::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $table->setMontant($updated->getMontant());
        $table->setTag($updated->getTag());
        $table->setDatetime($updated->getDatetime());
        $table->setUser($updated->getUser());

        $manager->flush();

        $data = $this->serializer->normalize($table, null, 
        ['attributes' => ['id', 'montant', 'tag', 'datetime', 'user']]);

        return new Response($this->serializer->serialize($data, 'json'));
    } 
}
